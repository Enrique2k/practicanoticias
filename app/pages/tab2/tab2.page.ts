import { Component } from '@angular/core';
import { ActionSheetController } from '@ionic/angular';
import { DataLocalService } from 'src/app/services/data-local.service';


@Component({
  selector: 'app-tab2',
  templateUrl: 'tab2.page.html',
  styleUrls: ['tab2.page.scss']
})
export class Tab2Page {
  variable: any;
  noticias: any;
  categoria: string;

  constructor(public actionSheetController: ActionSheetController , public data: DataLocalService) {
    this.categoria ="category=business&";
    this.getJson(this.categoria);
  }

  segmentChanged(ev: any) {
    //console.log('Segment changed', ev);
    this.variable = ev.detail.value;
    this.variable = "category="+this.variable+"&";
    this.getJson(this.variable);
    
  }

  async getJson(categoria){
    const respuesta = await fetch("https://newsapi.org/v2/top-headlines?"+categoria+"language=es&apiKey=05e56d9034cd43aa95b26f542540ce3d");
    this.noticias= await respuesta.json();
    //console.table(this.noticias)
    this.noticias = this.noticias.articles;

  }

  async lanzarMenu(noticia) {

    let storage = await this.data.storage.get('favoritos');

    let stat = false;
    let index;
    if(storage){
      for(var x  = 0 ; x < storage.length; x++){
        if(noticia.content == storage[x].content){
          stat = true;
          index = x;
          console.log(x);
        }
      }
    }
  
    console.log(storage);
    console.log(noticia);

    if(stat){
      const actionSheet = await this.actionSheetController.create({
        header: 'Options',
        cssClass: 'my-custom-class',
  
        buttons: [{
          text: 'Compartir',
          icon: 'share',
          handler: () => {
            console.log('Share clicked');
          }
        },{
          text: 'Eliminar Favorito',
          icon: 'star',
          handler: () => {
            
            this.data.eliminarNoticia(index);
           
          }
        }, {
          text: 'Cancel',
          icon: 'close',
          role: 'cancel',
          handler: () => {
            console.log('Cancel clicked');
          }
        }]
      });

    await actionSheet.present();

    const { role } = await actionSheet.onDidDismiss();
    console.log('onDidDismiss resolved with role', role);

    }else {
    const actionSheet = await this.actionSheetController.create({
      header: 'Options',
      cssClass: 'my-custom-class',

      buttons: [{
        text: 'Compartir',
        icon: 'share',
        handler: () => {
          console.log('Share clicked');
        }
      },{
        text: 'Favorito',
        icon: 'star',
        handler: () => {
      
          this.data.guardarNoticia(noticia);
         
        }
      }, {
        text: 'Cancel',
        icon: 'close',
        role: 'cancel',
        handler: () => {
          console.log('Cancel clicked');
        }
      }]
    });


    await actionSheet.present();

    const { role } = await actionSheet.onDidDismiss();
    console.log('onDidDismiss resolved with role', role);
  }


  }

  

}
