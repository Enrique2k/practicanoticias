import { Component, ViewChild } from '@angular/core';
import { ActionSheetController, IonInfiniteScroll } from '@ionic/angular';
import { DataLocalService } from 'src/app/services/data-local.service';
//import { InAppBrowser} from '@ionic-native/in-app-browser/ngx';

@Component({
  selector: 'app-tab1',
  templateUrl: 'tab1.page.html',
  styleUrls: ['tab1.page.scss']
})
export class Tab1Page {

  noticias: any;
  numNoticias: number;
  @ViewChild(IonInfiniteScroll) infiniteScroll: IonInfiniteScroll;
  //private iab: InAppBrowser
  constructor(public actionSheetController: ActionSheetController, public data: DataLocalService) {
    this.getJson();
  }
  

  async getJson(){
    const respuesta = await fetch("https://newsapi.org/v2/top-headlines?language=es&apiKey=05e56d9034cd43aa95b26f542540ce3d");
    this.noticias= await respuesta.json();
    this.noticias = this.noticias.articles;
    console.log(this.noticias);

  }
  loadData(event) {
    setTimeout(() => {
      console.log('Done');
      event.target.complete();

      // App logic to determine if all data is loaded
      // and disable the infinite scroll
      if (this.noticias == 1000) {
        event.target.disabled = true;
      }
    }, 500);
  }

  toggleInfiniteScroll() {
    this.infiniteScroll.disabled = !this.infiniteScroll.disabled;
  }
  openBrowser(){
  
  }

  async lanzarMenu(noticia) {
    const actionSheet = await this.actionSheetController.create({
      header: 'Options',
      cssClass: 'my-custom-class',

      buttons: [{
        text: 'Compartir',
        icon: 'share',
        handler: () => {
          console.log('Share clicked');
        }
      },{
        text: 'Favorito',
        icon: 'star',
        handler: () => {

          this.data.guardarNoticia(noticia);
          console.log(this.data.storage.get('favoritos'));
        }
      }, {
        text: 'Cancel',
        icon: 'close',
        role: 'cancel',
        handler: () => {
          console.log('Cancel clicked');
        }
      }]
    });
    await actionSheet.present();

    const { role } = await actionSheet.onDidDismiss();
    console.log('onDidDismiss resolved with role', role);
  }


}
