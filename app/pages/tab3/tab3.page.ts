import { Component } from '@angular/core';
import { DataLocalService } from 'src/app/services/data-local.service';

@Component({
  selector: 'app-tab3',
  templateUrl: 'tab3.page.html',
  styleUrls: ['tab3.page.scss']
})
export class Tab3Page {

  noticias: any;

  constructor(public data: DataLocalService) {

    this.cargar();
   
  }
  ionViewWillEnter(){
    this.cargar();
  }

  async cargar(){

    this.noticias = await this.data.storage.get('favoritos');
    console.log(this.noticias);
  }
}
