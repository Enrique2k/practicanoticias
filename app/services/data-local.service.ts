import { Injectable } from '@angular/core';
import { Storage } from '@ionic/storage-angular'

@Injectable({
  providedIn: 'root'
})

export class DataLocalService{

  noticias: any[] = [];

  constructor(public storage: Storage) {
    this.storage.create();
    this.cargarFavoritos();
  }

  async cargarFavoritos(){
    const favoritos = await this.storage.get('favoritos');
    if(favoritos){
      this.noticias = favoritos;
    }
  }

  public guardarNoticia(noticia: any){

    this.noticias.unshift(noticia);
    this.storage.set('favoritos' , this.noticias)
  }

  public eliminarNoticia(index){
    this.noticias.splice(index , 1);
    this.storage.set('favoritos' , this.noticias)

  }

}
